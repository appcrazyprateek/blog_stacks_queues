package blog.interfaces.stackandQueue;

public interface QueueImpl {

	public void enqueue(Object item);
	public Object dequeue();
}
